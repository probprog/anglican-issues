;; gorilla-repl.fileformat = 1

;; @@
(use 'nstools.ns)
(ns+ beta-flip
  (:like anglican-user.worksheet))
;; @@
;; =>
;;; {"type":"list-like","open":"","close":"","separator":"</pre><pre>","items":[{"type":"html","content":"<span class='clj-nil'>nil</span>","value":"nil"},{"type":"html","content":"<span class='clj-nil'>nil</span>","value":"nil"}],"value":"[nil,nil]"}
;; <=

;; **
;;; Parameterized model:
;; **

;; @@
(defquery one-flip [outcome]
  (let [theta (sample (beta 5 3))]
    (observe (flip theta) outcome)
    (predict :theta-gt-pt07 (> theta 0.7))))

;; @@
;; =>
;;; {"type":"html","content":"<span class='clj-var'>#&#x27;beta-flip/one-flip</span>","value":"#'beta-flip/one-flip"}
;; <=

;; **
;;; Samples from the model, using particle cascade:
;; **

;; @@
(def samples (doquery :pcascade one-flip [true] :number-of-particles 10 :number-of-threads 2000))

;; @@
;; =>
;;; {"type":"html","content":"<span class='clj-var'>#&#x27;beta-flip/samples</span>","value":"#'beta-flip/samples"}
;; <=

;; **
;;; Probabilities of each value (should be [[false 0.552] [true 0.448]]):
;; **

;; @@
(let [log-weights (->> samples
                   (take 50000)
                   (group-by (comp :theta-gt-pt07 get-predicts))
                   (map (fn [[value samples]]
                          [value (reduce log-sum-exp (map get-log-weight samples))])))
      log-total-weight (reduce log-sum-exp (map second log-weights))]
  
  (sort-by first (map (fn [[value log-weight]] [value (exp (- log-weight log-total-weight))]) log-weights)))
;; @@
;; =>
;;; {"type":"list-like","open":"<span class='clj-list'>(</span>","close":"<span class='clj-list'>)</span>","separator":" ","items":[{"type":"list-like","open":"<span class='clj-vector'>[</span>","close":"<span class='clj-vector'>]</span>","separator":" ","items":[{"type":"html","content":"<span class='clj-unkown'>false</span>","value":"false"},{"type":"html","content":"<span class='clj-double'>0.5520592678352071</span>","value":"0.5520592678352071"}],"value":"[false 0.5520592678352071]"},{"type":"list-like","open":"<span class='clj-vector'>[</span>","close":"<span class='clj-vector'>]</span>","separator":" ","items":[{"type":"html","content":"<span class='clj-unkown'>true</span>","value":"true"},{"type":"html","content":"<span class='clj-double'>0.447940732164792</span>","value":"0.447940732164792"}],"value":"[true 0.447940732164792]"}],"value":"([false 0.5520592678352071] [true 0.447940732164792])"}
;; <=
