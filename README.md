# Minimal Gorilla REPL worksheets (or standalone programs) for [Anglican](https://bitbucket.org/probprog/anglican) problem reports.

When you report a bug in Anglican, create a subdirectory with the ticket in number in either `worksheets` or `programs` and put a source code example (either a worksheet or a standalone program)into that directory. See worksheets/61/beta-flip.clj for an example.

If you don't have write access to the repository but believe you should (or might) have, ask one of repository admins and most probably you'll get it.